import java.util.Random;
/**
 * This class is the AI, the play method will be overloaded
 */
public class AI extends Player {

    Random random = new Random();

    public AI() {
        System.out.println("AI constructor");
        this.name="bob";
        this.symbol='X';
    }

    @Override
    public int play() {

        int cellAJouer;
        Cell c;
        // parameter 1 to verify if the AI player can win
        if (( c = Game.playerCanWin(1) ) != null) {
            cellAJouer=c.getId();
            System.out.println("AI can win");
            return cellAJouer;
        }
        // parameter 0 to verify if the Human player can win
        if (( c = Game.playerCanWin(0) ) != null) {
            cellAJouer=c.getId();
            System.out.println("AI is blocking");
            return cellAJouer;
        }
        if (Game.isCenterFree()) {
            System.out.println("AI is playing in the center");
            cellAJouer = 4;
            return cellAJouer;
        }
        else {
            System.out.println("AI is playing randomly");
            cellAJouer = random.nextInt(9);
            return cellAJouer;
        }
    }
}
