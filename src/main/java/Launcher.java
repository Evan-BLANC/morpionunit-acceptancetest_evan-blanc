/**
 * This is the launcher of the game
 */
public class Launcher {

    public static void main(String[] args) {


        Player p1 = new Human();
        Player p2 = new AI();

        Game game = new Game(p1,p2, false);
    }
}
