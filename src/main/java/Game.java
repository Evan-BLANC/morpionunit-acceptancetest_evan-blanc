import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This class represent the game with :
 *
 *      -The list of cells
 *      -The number of free cells
 *      -The list of the players ( shuffle this list to randomize the first player )
 *      -The number of turns ( player who play : listPlayer.get(numberOfTurns % 2) )
 */
public class Game {

    private static List<Cell> listCells = new ArrayList<Cell>();
    private static List<Player> listPlayers = new ArrayList<Player>();
    private int nbrTurns;
    private int nbrFreeCells;

    /**
     * The advantage with this methods is that you can play 1vs1 or 1vsAI, because AI is a subclass of Player
     */
    public Game(Player p1, Player p2, boolean test) {

        listPlayers.add(p1);
        listPlayers.add(p2);
        init();
        if ( !test ) start();
    }

    /**
     * Init the grid of the game
     */
    private void init() {

        nbrTurns=0;
        nbrFreeCells=9;
        for (int i=0; i<9; i++) { listCells.add(new Cell(i)); }
        Collections.shuffle(listPlayers);

        System.out.println("Voici comment cela fonctionne : chaque cellule est représentée par un chiffre comme suit");
        System.out.println("1 | 2 | 3");
        System.out.println("4 | 5 | 6");
        System.out.println("7 | 8 | 9");
    }

    /**
     * The game begin with the first player, who has been chosen randomly in init()
     */
    private void start() {

        Player actualPlayer=null;
        do {
            actualPlayer = listPlayers.get(nbrTurns%2); // Will be 0 or 1, P1 or P2, this change each turn
            System.out.println(actualPlayer.getName()+", c'est à vous de jouer !");
            int chosenCell = actualPlayer.play();

            // On verifie si la cell est free, sinon on rappelle play en affichant un message d'erreur
            while (!listCells.get(chosenCell).isFree()) {

                System.out.println("Vous devez chosir une case libre !");
                chosenCell = actualPlayer.play();
            }

            // On change la valeur de Player.listOwnedCells à l'indice chosenCell
            actualPlayer.ownACell(chosenCell);
            nbrFreeCells--;

            // On update la grille avec le signe du joueur
            listCells.get(chosenCell).setSymbol(actualPlayer.getSymbol());
            listCells.get(chosenCell).setNotFree();

            nbrTurns++;
        }while (nbrFreeCells>0 && !actualPlayer.hasWon());

        printGrid();

        for (Player p : listPlayers) {
            if (p.hasWon())  {
                System.out.println(p.getName()+" est le vainqueur, bravo à lui!");
                System.exit(0);
            }
        }

        System.out.println("Egalite, ca sera pour une prochaine fois");
        System.exit(0);
    }

    /**
     * Called by play() in Player, this method print the grid using the symbol of each cell
     */
    public static void printGrid() {

        System.out.println("1 | 2 | 3");
        System.out.println("4 | 5 | 6");
        System.out.println("7 | 8 | 9");

        System.out.println(listCells.get(0)+" | "+listCells.get(1)+" | "+listCells.get(2));
        System.out.println(listCells.get(3)+" | "+listCells.get(4)+" | "+listCells.get(5));
        System.out.println(listCells.get(6)+" | "+listCells.get(7)+" | "+listCells.get(8));
    }

    /**
     * Called by the AI to know if the center is free
     */
    public static boolean isCenterFree() {
        return listCells.get(4).isFree();
    }

    /**
     * Called by the AI to know if it or the human player can win next turn
     * @return Cell cellGagnante if it's winnable, null if it isn't
     *
     * ( bon c'est super moche mais là j'avais pas d'idée )
    */
    public static Cell playerCanWin(int player) {

        Player p;
        List<Cell> combination = new ArrayList<Cell>();
        int nbrCellsOfTheHumanPlayerInCombination=0;
        Cell freeCell=null;

        if ( player == 0 ) {
            if (listPlayers.get(0) instanceof Human) {
                p = listPlayers.get(0);
            } else {
                p = listPlayers.get(1);
            }
        }else {
            if (listPlayers.get(0) instanceof AI) {
                p = listPlayers.get(0);
            } else {
                p = listPlayers.get(1);
            }
        }
        // Lignes

        combination.add(listCells.get(0));
        combination.add(listCells.get(1));
        combination.add(listCells.get(2));

        for(Cell c : combination) {

            if (c.getSymbol() == p.getSymbol() ) {
                nbrCellsOfTheHumanPlayerInCombination++;
            }else if (c.isFree()) {
                freeCell=c;
            }
        }

        if (nbrCellsOfTheHumanPlayerInCombination == 2) return freeCell;
        combination.clear();
        nbrCellsOfTheHumanPlayerInCombination=0;
        freeCell=null;

        combination.add(listCells.get(3));
        combination.add(listCells.get(4));
        combination.add(listCells.get(5));

        for(Cell c : combination) {

            if (c.getSymbol() == p.getSymbol() ) {
                nbrCellsOfTheHumanPlayerInCombination++;
            }else if (c.isFree()) {
                freeCell=c;
            }
        }

        if (nbrCellsOfTheHumanPlayerInCombination == 2) return freeCell;
        combination.clear();
        nbrCellsOfTheHumanPlayerInCombination=0;
        freeCell=null;


        combination.add(listCells.get(6));
        combination.add(listCells.get(7));
        combination.add(listCells.get(8));

        for(Cell c : combination) {

            if (c.getSymbol() == p.getSymbol() ) {
                nbrCellsOfTheHumanPlayerInCombination++;
            }else if (c.isFree()) {
                freeCell=c;
            }
        }

        if (nbrCellsOfTheHumanPlayerInCombination == 2) return freeCell;
        combination.clear();
        nbrCellsOfTheHumanPlayerInCombination=0;
        freeCell=null;


        //Colonnes

        combination.add(listCells.get(0));
        combination.add(listCells.get(3));
        combination.add(listCells.get(6));

        for(Cell c : combination) {
<<<<<<< refs/remotes/origin/master

=======
            System.out.println("P : "+p.getSymbol());
            System.out.println("C : "+c.getSymbol());
>>>>>>> AI knows how to win
            if (c.getSymbol() == p.getSymbol() ) {
                nbrCellsOfTheHumanPlayerInCombination++;
            }else if (c.isFree()) {
                freeCell=c;
            }
        }

        if (nbrCellsOfTheHumanPlayerInCombination == 2) {
            assert freeCell != null;
            System.out.println(freeCell.getId());
            return freeCell;
        }
        combination.clear();
        nbrCellsOfTheHumanPlayerInCombination=0;
        freeCell=null;


        combination.add(listCells.get(1));
        combination.add(listCells.get(4));
        combination.add(listCells.get(7));

        for(Cell c : combination) {

            if (c.getSymbol() == p.getSymbol() ) {
                nbrCellsOfTheHumanPlayerInCombination++;
            }else if (c.isFree()) {
                freeCell=c;
            }
        }

        if (nbrCellsOfTheHumanPlayerInCombination == 2) return freeCell;
        combination.clear();
        nbrCellsOfTheHumanPlayerInCombination=0;
        freeCell=null;


        combination.add(listCells.get(2));
        combination.add(listCells.get(5));
        combination.add(listCells.get(8));

        for(Cell c : combination) {

            if (c.getSymbol() == p.getSymbol() ) {
                nbrCellsOfTheHumanPlayerInCombination++;
            }else if (c.isFree()) {
                freeCell=c;
            }
        }

        if (nbrCellsOfTheHumanPlayerInCombination == 2) return freeCell;
        combination.clear();
        nbrCellsOfTheHumanPlayerInCombination=0;
        freeCell=null;


        // Diagonales

        combination.add(listCells.get(0));
        combination.add(listCells.get(4));
        combination.add(listCells.get(8));

        for(Cell c : combination) {

            if (c.getSymbol() == p.getSymbol() ) {
                nbrCellsOfTheHumanPlayerInCombination++;
            }else if (c.isFree()) {
                freeCell=c;
            }
        }

        if (nbrCellsOfTheHumanPlayerInCombination == 2) return freeCell;
        combination.clear();
        nbrCellsOfTheHumanPlayerInCombination=0;
        freeCell=null;


        combination.add(listCells.get(2));
        combination.add(listCells.get(4));
        combination.add(listCells.get(6));

        for(Cell c : combination) {

            if (c.getSymbol() == p.getSymbol() ) {
                nbrCellsOfTheHumanPlayerInCombination++;
            }else if (c.isFree()) {
                freeCell=c;
            }
        }

        if (nbrCellsOfTheHumanPlayerInCombination == 2) return freeCell;
        combination.clear();

        return null;
    }

    //------------------------------------------------------------------------------------------------------------------


    public static List<Cell> getListCells() {
        return listCells;
    }
}
