/**
 * This class represent a player with :
 *
 *      -A name
 *      -A symbol
 *      -A boolean for each cell of the game (9)
 */
public abstract class Player {

    protected String name;
    protected char symbol;
    protected boolean[] ownedCells = new boolean[]{false,false,false,false,false,false,false,false,false};


    /**
     * This methods ask the player for a name and a symbol
     */
    public void editPlayer() {

        System.out.println("Bonjour, au fait, quel est votre nom ?");
        this.name=application.Lire.S();
        do {
            System.out.println("Avec quel symbol souhaitez vous jouer ? X est le symbol de l'IA");
            this.symbol = application.Lire.c();
        }while(this.symbol=='X' && this.symbol=='x');

        System.out.println("très bien "+this.name+", bonne chance à vous \n");
    }

    /**
     * This is the play method, in this case, the player is human, we need to ask him to play
     * This method will be overloaded in AI because it's play itself
     */
    public int play() {

        return -1;
    }

    /**
     * The player own a cell, the linked boolean will become true
     * @param id
     */
    public void ownACell(int id) {

        ownedCells[id]=true;
    }

    /**
     * This method will be called to check if the player has won
     * @return true if the play has won or not
     */
    public boolean hasWon() {

        return ownedCells[0] && ownedCells[1] && ownedCells[2] ||
                ownedCells[3] && ownedCells[4] && ownedCells[5] ||
                ownedCells[6] && ownedCells[7] && ownedCells[8] ||

                ownedCells[0] && ownedCells[3] && ownedCells[6] ||
                ownedCells[1] && ownedCells[4] && ownedCells[7] ||
                ownedCells[2] && ownedCells[5] && ownedCells[8] ||

                ownedCells[0] && ownedCells[4] && ownedCells[8] ||
                ownedCells[2] && ownedCells[4] && ownedCells[6];
    }


    //------------------------------------------------------------------------------------------------------------------

    public String getName() {
        return name;
    }

    public char getSymbol() {
        return this.symbol;
    }
}