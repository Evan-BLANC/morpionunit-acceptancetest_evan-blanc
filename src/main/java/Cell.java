/**
 * This class represent a cell in the game
 * It has an ID
 * and a symbol which is the symbol of the player who clicked this cell
 */
public class Cell {

    private int id;
    private char symbol;
    private boolean free;

    public Cell(int id) {

        free=true;
        symbol=' ';
        this.id=id;
    }

    @Override
    public String toString() {
        return symbol+"";
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     *
     * @param symbol -> Player.getSymbol()
     */
    public void setSymbol(char symbol) {
        this.symbol=symbol;
    }

    public char getSymbol() {
        return symbol;
    }

    public int getId() {
        return id;
    }

    public boolean isFree() {
        return free;
    }

    public void setNotFree() {

        this.free = false;
    }

    public void selectCell() {
        free=false;
    }
}
