import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

public class MorpionUnitTest {

    private AI ai = new AI();
    private Human human = new Human('O');
    private Game game;


    @Test
    public void aiPlaysInTheCenterIfItCan() {
        game = new Game(ai, human, true);
        int cellJouee = ai.play();
        Assert.assertEquals(cellJouee, 4);
    }

    @Test
    public void aiBlockIfItNeedsTo() {

        game = new Game(ai, human, true);
        Game.getListCells().get(0).setSymbol(human.getSymbol());
        Game.getListCells().get(4).setSymbol(human.getSymbol());
        int cellJouee = ai.play();
        Assert.assertEquals(cellJouee, 8);
    }

    @Test
    public void aiWinIfItCan() {

        game = new Game(ai, human, true);
        Game.getListCells().get(0).setSymbol(ai.getSymbol());
        Game.getListCells().get(4).setSymbol(ai.getSymbol());
        int cellJouee = ai.play();
        Assert.assertEquals(cellJouee, 8);
    }

    @Test
    public void aiWinInLine() {

        ai = Mockito.mock(AI.class);
        Mockito.when(ai.play()).thenReturn(0,1,2);
        game = new Game(ai, human, true);
        ai.play();
        ai.play();
        ai.play();
        Assert.assertTrue(ai.hasWhon(),true);
    }

    @Test
    public void aiWinInColumn() {

        ai = Mockito.mock(AI.class);
        Mockito.when(ai.play()).thenReturn(0,3,6);
        game = new Game(ai, human, true);
        ai.play();
        ai.play();
        ai.play();
        Assert.assertTrue(ai.hasWhon(),true);
    }

    @Test
    public void aiWinInDiagon() {

        ai = Mockito.mock(AI.class);
        Mockito.when(ai.play()).thenReturn(0,4,8);
        game = new Game(ai, human, true);
        ai.play();
        ai.play();
        ai.play();
        Assert.assertTrue(ai.hasWhon(),true);
    }
}
