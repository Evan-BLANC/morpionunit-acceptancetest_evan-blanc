Feature: Gagner une partie

  Scenario: L'ordinateur gagne sur une ligne avec les X
    Given La grille contient une ligne avec 2 X
    And La dernière case est <libre>
    When Ordinateur joue
    Then Ordinateur joue une X en case <libre>

  Scenario: L'ordinateur gagne sur une colonne avec les X
    Given La grille contient une colonne avec 2 X
    And La dernière case est <libre>
    When Ordinateur joue
    Then Ordinateur joue une X en case <libre>

  Scenario: L'ordinateur gagne sur une diagonale avec les X
    Given La grille contient une diagonale avec 2 X
    And La dernière case est <libre>
    When Ordinateur joue
    Then Ordinateur joue une X en case <libre>