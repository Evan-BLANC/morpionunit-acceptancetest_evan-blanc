Feature Empêcher le joueur de gagner

  Scenario: Le joueur gagne au prochain tour en ligne
    Given C'est le tour de l'IA
    And C'est la seule combinaison gagnante du joueur
    When L'IA joue sur la dernière case de la ligne en question
    Then La partie continue sans victoire du joueur immédiate

  Scenario: Le joueur gagne au prochain tour en colonne
    Given C'est le tour de l'IA
    And C'est la seule combinaison gagnante du joueur
    When L'IA joue sur la dernière case de la colonne en question
    Then La partie continue sans victoire du joueur immédiate

  Scenario: Le joueur gagne au prochain tour en diagonale
    Given C'est le tour de l'IA
    And C'est la seule combinaison gagnante du joueur
    When L'IA joue sur la dernière case de la diagonale en question
    Then La partie continue sans victoire du joueur immédiate

  Scenario: Le joueur gagne au prochain tour en ligne et en colonne et en diagonale
    Given C'est le tour de l'IA
    And Ce n'est pas la seule combinaison gagnante du joueur
    When L'IA joue sur la dernière case de la ligne / colonne / diagnonale en question
    Then La partie se termine sur une victoire du joueur