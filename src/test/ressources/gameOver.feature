Feature La partie est terminée

  Scenario: Match null
    Given C'est le tour de l'IA
    And Il n'y a plus qu'une case libre dans la grille
    And Il n'y a pas de combinaison gagnante pour l'IA
    When L'IA joue sur la dernière case possible
    Then La partie se termine en match null

  Scenario: Victoire de l'IA
    Given C'est le tour de l'IA
    And Il y a une combinaison gagnante pour l'IA
    When L'IA joue sur la bonne case
    Then La partie se termine et l'IA gagne
